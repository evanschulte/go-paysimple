module gitlab.com/evanschulte/go-paysimple

go 1.14

require (
	github.com/codegangsta/envy v0.0.0-20141216192214-4b78388c8ce4
	github.com/stretchr/testify v1.7.0
)
