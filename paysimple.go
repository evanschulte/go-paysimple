package paysimple

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
)

type Api struct {
	baseURL url.URL
	config  Config
	backend backend

	// Resource endpoints
	Accounts  *Accounts
	Payments  *Payments
	Customers *Customers
}

func (api *Api) decodeError(resp *http.Response) error {
	var empty Empty
	defer resp.Body.Close()
	if err := json.NewDecoder(resp.Body).Decode(&empty); err != nil {
		return fmt.Errorf("paysimple: failed to decode error %v: %s - %s", resp.StatusCode, err, resp.Body)
	}
	// Set the status code on the error itself
	if empty.Meta.Errors != nil {
		empty.Meta.Errors.StatusCode = empty.Meta.HttpStatusCode
	}
	return empty.Meta.Errors
}

func (api *Api) request(method string, uri *url.URL, v interface{}) (*http.Request, error) {
	content, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, uri.String(), bytes.NewBuffer(content))
	if err != nil {
		return nil, err
	}

	// Set the content type - THIS IS IMPORTANT BECAUSE JAVA/SPRING SUCKS
	req.Header.Set("Content-Type", "application/json")

	// Add the authorization header
	auth := CreateAuthorization(api.config.Username, api.config.SecretKey)
	req.Header.Set("Authorization", auth)
	return req, nil
}

func (api *Api) Get(uri *url.URL) (*http.Request, error) {
	return api.request("GET", uri, nil)
}

func (api *Api) Post(uri *url.URL, v interface{}) (*http.Request, error) {
	return api.request("POST", uri, v)
}

func (api *Api) Put(uri *url.URL, v interface{}) (*http.Request, error) {
	return api.request("PUT", uri, v)
}

func (api *Api) URL(path ...string) *url.URL {
	api.baseURL.Path = strings.Join(path, "/")
	return &api.baseURL
}

type Config struct {
	Username, SecretKey string
}

func (config Config) IsValid() bool {
	// TODO better validation - expected length / prefix?
	return config.Username != "" && config.SecretKey != ""
}

func Env() (config Config) {
	config.Username = os.Getenv("PAYSIMPLE_USER")
	config.SecretKey = os.Getenv("PAYSIMPLE_SECRET")
	return
}

func create(baseURL url.URL) *Api {
	config := Env()
	if !config.IsValid() {
		panic("Failed to parse environmental variables - are they set?")
	}
	api := &Api{
		baseURL: baseURL,
		config:  config,
		backend: &http.Client{},
	}

	// Connect endpoints
	api.Accounts = &Accounts{api: api}
	api.Payments = &Payments{api: api}
	api.Customers = &Customers{api: api}
	return api
}

func API() *Api {
	return create(url.URL{Scheme: "https", Host: "api.paysimple.com"})
}

func Sandbox() *Api {
	return create(url.URL{Scheme: "https", Host: "sandbox-api.paysimple.com"})
}

// test is a test backend for internal httptest
func test(uri string) *Api {
	parsed, err := url.Parse(uri)
	if err != nil {
		log.Panic(err)
	}
	return create(*parsed)
}
